//
//  TREEExportTCX.h
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 19.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

//  Logic unit tests contain unit test code that is designed to be linked into an independent test executable.

#import <SenTestingKit/SenTestingKit.h>

@interface TREExportTCXTest : SenTestCase

@end
