//
//  TREActivity.h
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 18.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TRELap.h"

@interface TREActivity : NSObject {
    NSDate* date;
    NSNumber* type;
    NSArray* laps;
}

@property (retain) NSDate* date;
@property (retain) NSNumber* type;
@property (retain) NSArray* laps;

@end
