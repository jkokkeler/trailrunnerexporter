//
//  TREImport.h
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 18.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TREWaypoint.h"
#import "TRELap.h"
#import "TREActivity.h"

@interface TREImport : NSObject

+(int)readIndexFromValue:(id)inObj;
+(TREWaypoint *)parseWaypoint:(NSDictionary *)input;
+(TRELap *)parseLap:(NSDictionary *)input fromTrack:(NSArray *)track;
+(TREActivity *)parseActivity:(NSArray *)track withDate:(NSDate *)date;

@end
