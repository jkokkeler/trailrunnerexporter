//
//  TREImport.m
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 18.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TREImport.h"
#import "TREWaypoint.h"
#import "TRELap.h"
#import "TREActivity.h"
#import "TrailRunnerExporterAppDelegate.h"

@implementation TREImport

static double NULLVALUE = -9999999.999999;

+ (int)readIndexFromValue:(id)inObj {
    
    NSString *inString = [NSString stringWithFormat:@"%@", inObj];
    NSRange rangeBegin = [inString rangeOfString:@"{value = "];
    NSRange rangeEnd = [inString rangeOfString:@"}"];
    unsigned long location = rangeBegin.location + rangeBegin.length;
    unsigned long length = rangeEnd.location - location;
    NSRange range = NSMakeRange(location, length);
    NSString *value = [inString substringWithRange:range];
    return [value intValue];

}

+(TREWaypoint *)parseWaypoint:(NSDictionary *)input {

    TREWaypoint *ret = [[TREWaypoint alloc] init];

    [ret setDate:[NSDate dateWithTimeIntervalSinceReferenceDate:[[input objectForKey:@"time"] intValue]]];
    [ret setLat:[NSNumber numberWithDouble:[[input objectForKey:@"lat"] doubleValue]]];
    [ret setLon:[NSNumber numberWithDouble:[[input objectForKey:@"lon"] doubleValue]]];
    [ret setAlt:[NSNumber numberWithDouble:[[input objectForKey:@"alt"] doubleValue]]];
    [ret setDistance:[NSNumber numberWithDouble:[[input objectForKey:@"distance"] doubleValue]]];
    if ([input objectForKey:@"heartRate"] && [[input objectForKey:@"heartRate"] doubleValue] != 0){
        [ret setHeartRate:[NSNumber numberWithDouble:[[input objectForKey:@"heartRate"] doubleValue]]];
    }
    if ([input objectForKey:@"cadence"] && [[input objectForKey:@"cadence"] doubleValue] != 0){
        [ret setCadence:[NSNumber numberWithDouble:[[input objectForKey:@"cadence"] doubleValue]]];
    }
    
    return ret;

}

+(TRELap *)parseLap:(NSDictionary *)input fromTrack:(NSArray *)track {
    
    TRELap *ret = [[TRELap alloc] init];

    if ([[input objectForKey:@"duration"] doubleValue] != NULLVALUE) {
        [ret setDuration:[input objectForKey:@"duration"]];
    }
    if ([[input objectForKey:@"distance"] doubleValue] != NULLVALUE) {
        [ret setDistance:[input objectForKey:@"distance"]];
    }
    if ([[input objectForKey:@"avgSpeed"] doubleValue] != NULLVALUE) {
        [ret setAvgSpeed:[input objectForKey:@"avgSpeed"]];
    }
    if ([[input objectForKey:@"maxSpeed"] doubleValue] != NULLVALUE) {
        [ret setMaxSpeed:[input objectForKey:@"maxSpeed"]];
    }
    if ([[input objectForKey:@"calories"] doubleValue] != NULLVALUE) {
        [ret setCalories:[input objectForKey:@"calories"]];
    }
    if ([[input objectForKey:@"avgHeartRate"] doubleValue] != NULLVALUE) {
        [ret setAvgHeartRate:[input objectForKey:@"avgHeartRate"]];
    }
    if ([[input objectForKey:@"maxHeartRate"] doubleValue] != NULLVALUE) {
        [ret setMaxHeartRate:[input objectForKey:@"maxHeartRate"]];
    }
    if ([[input objectForKey:@"avgCadence"] doubleValue] != NULLVALUE) {
        [ret setAvgCadence:[input objectForKey:@"avgCadence"]];
    }
    
    NSArray *trackRaw = [[track objectAtIndex:[self readIndexFromValue:[input objectForKey:@"trackPoints"]]] objectForKey:@"NS.objects"];
    NSMutableArray *trackRes = [[NSMutableArray alloc] init];
    for (id trackWptIdx in trackRaw) {
        [trackRes addObject:[self parseWaypoint:[track objectAtIndex:[self readIndexFromValue:trackWptIdx]]]];
    }
    [ret setTrack:trackRes];
         
    return ret;
    
}

+(TREActivity *)parseActivity:(NSArray *)track withDate:(NSDate *)date {
    
    NSMutableArray *lapsRes = [[NSMutableArray alloc] init];
    NSDictionary *index = [track objectAtIndex:1];

    TREActivity *ret = [[TREActivity alloc] init];

    [ret setDate:date];
    [ret setType:[NSNumber numberWithInt:[[index objectForKey:@"sportActivity"] intValue]]];

    /*for (id key in index) {
        NSLog(@"key=%@ value=%@", key, [index objectForKey:key]);  
    }*/

    int lapsIdx = [self readIndexFromValue:[index objectForKey:@"laps"]];
    if (lapsIdx > 0) {
        NSArray *lapsRaw = [[track objectAtIndex:lapsIdx] objectForKey:@"NS.objects"];
        for (id lapIdx in lapsRaw) {
            [lapsRes addObject:[self parseLap:[track objectAtIndex:[self readIndexFromValue:lapIdx]] fromTrack:track]];
        }
    } else {
        [lapsRes addObject:[self parseLap:index fromTrack:track]];
    }
    
    [ret setLaps:lapsRes];
    
    return ret;
    
}

@end
