//
//  TREWaypoint.h
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 18.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TREWaypoint : NSObject {
    NSDate *date;
    NSNumber *lat;
    NSNumber *lon;
    NSNumber *alt;
    NSNumber *distance;
    NSNumber *heartRate;
    NSNumber *cadence;
}

@property (retain) NSDate* date;
@property (retain) NSNumber* lat;
@property (retain) NSNumber* lon;
@property (retain) NSNumber* alt;
@property (retain) NSNumber* distance;
@property (retain) NSNumber* heartRate;
@property (retain) NSNumber* cadence;


@end
