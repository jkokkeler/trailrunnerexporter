//
//  TREExportTCX.h
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 18.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TREWaypoint.h"
#import "TRELap.h"
#import "TREActivity.h"

@interface TREExportTCX : NSObject {
}

+(NSDateFormatter *)dateFormatter;
+(NSString *)serializeWaypoint:(TREWaypoint*)wpt;
+(NSString *)serializeLap:(TRELap*)lap;
+(NSString *)serializeActivity:(TREActivity*)activity;

@end
