//
//  TRELap.m
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 18.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TRELap.h"

@implementation TRELap

@synthesize duration;
@synthesize distance;
@synthesize avgSpeed;
@synthesize maxSpeed;
@synthesize calories;
@synthesize avgHeartRate;
@synthesize maxHeartRate;
@synthesize avgCadence;
@synthesize track;

@end
