//
//  TREImportTest.m
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 19.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TREImportTest.h"
#import "TREImport.h"

@implementation TREImportTest

// All code under test must be linked into the Unit Test bundle
- (void)testreadIndexFromValue {
    NSString *exampleString = @"<CFKeyedArchiverUID 0x109cdda60 [0x7fff7c97dfa0]>{value = 435}";
    int value = [TREImport readIndexFromValue:exampleString];
    STAssertEquals(435, value , @"RegEx doesn't work");
}

- (void)testWaypoint
{
    
    NSMutableDictionary *dict = [[NSMutableDictionary alloc]initWithCapacity:10];
    [dict setObject:@"245124521" forKey:@"time"];
    [dict setObject:@"49.10" forKey:@"lat"];
    [dict setObject:@"12.02" forKey:@"lon"];
    [dict setObject:@"100" forKey:@"alt"];
    [dict setObject:@"10" forKey:@"distance"];
    
    TREWaypoint *wpt = [TREImport parseWaypoint:dict];
    STAssertEqualObjects([NSNumber numberWithDouble:49.10], [wpt lat], @"lat did not match");
    STAssertEqualObjects([NSNumber numberWithDouble:12.02], [wpt lon], @"lon did not match");
    STAssertEqualObjects([NSNumber numberWithDouble:100], [wpt alt], @"alt did not match");
    STAssertEqualObjects([NSNumber numberWithDouble:10], [wpt distance], @"distance did not match");
    STAssertTrue([wpt heartRate] == NULL, @"heartRate is not null");
    
}

@end
