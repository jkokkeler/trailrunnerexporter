//
//  TrailRunnerExporterAppDelegate.m
//  TrailRunnerExporter
//
//  Created by Jochen Kähler on 17.06.12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TrailRunnerExporterAppDelegate.h"
#import "/usr/include/sqlite3.h"
#import "TREImport.h"
#import "TREExportTCX.h"

@implementation TrailRunnerExporterAppDelegate

@synthesize window = _window;
@synthesize inFileName = _inFileName;
@synthesize outFileName = _outFileName;
@synthesize goButton = _goButton;
@synthesize processBar = _processBar;

static int MAX_RETRYS = 10;

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {

    [_inFileName setStringValue:[NSString stringWithFormat:@"%@/Library/Application Support/TrailRunner/ActivityStore.sqlite", NSHomeDirectory()]];
    [_outFileName setStringValue:[NSString stringWithFormat:@"%@/", NSHomeDirectory()]];
    
}

- (IBAction)chooseInFile:(id)sender {

    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    [openDlg setDirectoryURL:[NSURL URLWithString:[NSString stringWithFormat:@"file://localhost/%@", [_inFileName stringValue]] ]];
    [openDlg setCanCreateDirectories:YES];
    [openDlg setCanChooseFiles:YES];
    
    if ( [openDlg runModal] == NSOKButton ) {
        [_inFileName setStringValue:[[[openDlg URLs] objectAtIndex:0] path]];
    }

}

- (IBAction)chooseOutDir:(id)sender {
    
    NSOpenPanel* openDlg = [NSOpenPanel openPanel];
    [openDlg setCanChooseFiles:NO];
    [openDlg setCanChooseDirectories:YES];
    [openDlg setCanCreateDirectories:YES];
    
    if ( [openDlg runModal] == NSOKButton ) {
        [_outFileName setStringValue:[[[openDlg URLs] objectAtIndex:0] path]];
    }
    
}

- (void)alert:(NSString*)msg {
    NSAlert *alert = [[NSAlert alloc] init];
    [alert addButtonWithTitle:@"OK"];
    [alert setMessageText:@"Error!"];
    [alert setInformativeText:msg];
    [alert setAlertStyle:NSWarningAlertStyle];
    [alert beginSheetModalForWindow:_window modalDelegate:self didEndSelector:@selector(alertDidEnd:returnCode:contextInfo:) contextInfo:nil];    
}

- (void)finished {
    NSAlert *finished = [[NSAlert alloc] init];
    [finished addButtonWithTitle:@"Quit"];
    [finished addButtonWithTitle:@"OK"];
    [finished setMessageText:@"Finished!"];
    [finished setInformativeText:@"Finished successfully!"];
    [finished setAlertStyle:NSWarningAlertStyle];
    [finished beginSheetModalForWindow:_window modalDelegate:self didEndSelector:@selector(finishedDidEnd:returnCode:contextInfo:) contextInfo:nil];    
}


- (void)finishedDidEnd:(NSAlert *)alert returnCode:(NSInteger)returnCode contextInfo:(void *)contextInfo {
    if (returnCode == 1000) {
        [NSApp terminate: nil];
    }
}

- (IBAction)exportActivities:(id)sender {
    
    //[self alert:@"You hit the go button!"];
    int res = SQLITE_ERROR;
    sqlite3 *database;

    res = sqlite3_open([[_inFileName stringValue] UTF8String], &database);
    if (res != SQLITE_OK) {
        [self alert:@"Could not open activity store"];
        return;
    }

    sqlite3_stmt *statementCount;
    NSString *queryCount = @"SELECT COUNT(1) FROM ZASWORKOUTBASE WHERE ZWORKOUT IS NOT NULL";
    res = sqlite3_prepare_v2 (database, [queryCount UTF8String], -1, &statementCount, nil);
    if (res == SQLITE_OK) {
        if(sqlite3_step(statementCount) == SQLITE_ROW) {
            [_processBar setMaxValue:sqlite3_column_int(statementCount, 0)];
            [_processBar setDoubleValue:0];
        }
    } else {
        [self alert:@"Could not execute count query on activity store"];
        return;
    }
    sqlite3_finalize(statementCount);
    
    sqlite3_stmt *statement;
    NSString *query = @"SELECT Z_PK, ZDATE, ZWORKOUT FROM ZASWORKOUTBASE WHERE ZWORKOUT IS NOT NULL";
    res = sqlite3_prepare_v2 (database, [query UTF8String], -1, &statement, nil);
    
    if (res == SQLITE_OK) {
        
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss'Z'"];

        NSDateFormatter *dateFormatterFilename = [[NSDateFormatter alloc] init];
        [dateFormatterFilename setDateFormat:@"yyyy-MM-dd'-'HHmmss"];

        while(sqlite3_step(statement) == SQLITE_ROW) {

            //int z_pk = sqlite3_column_int(statement, 0);
            
            int zdate = sqlite3_column_int(statement, 1);
            NSDate *date = [NSDate dateWithTimeIntervalSinceReferenceDate:zdate];
            NSString *dateForFilename = [dateFormatterFilename stringFromDate:date];

            int len = sqlite3_column_bytes(statement, 2);
            NSData *data = [[NSData alloc] initWithBytes: sqlite3_column_blob(statement, 2) length: len];
            //NSLog(@"processing z_pk=%d date=%@", z_pk, [dateFormatter stringFromDate:date]);

            NSString *error;
            NSDictionary *temp = [NSPropertyListSerialization propertyListFromData:data mutabilityOption:NSPropertyListImmutable format:NULL errorDescription:&error];
            if (error != NULL) {
                [self alert:[NSString stringWithFormat:@"deseralizing error: %@", error]];
                NSLog(@"deseralizing error: %@", error);
                continue;
            }
            
            NSMutableString *xmlData = [[NSMutableString alloc] initWithString:@""];
            [xmlData appendString:@"<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"];
            [xmlData appendString:@"<TrainingCenterDatabase xmlns:ns5=\"http://www.garmin.com/xmlschemas/ActivityGoals/v1\" xmlns:ns3=\"http://www.garmin.com/xmlschemas/ActivityExtension/v2\" xmlns:ns2=\"http://www.garmin.com/xmlschemas/UserProfile/v2\" xmlns=\"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:ns4=\"http://www.garmin.com/xmlschemas/ProfileExtension/v1\" xsi:schemaLocation=\"http://www.garmin.com/xmlschemas/TrainingCenterDatabase/v2 http://www.garmin.com/xmlschemas/TrainingCenterDatabasev2.xsd\">\n"];
            [xmlData appendString:@"\t<Activities>\n"];
            
            NSArray *track = [temp objectForKey:@"$objects"];

            TREActivity *activity = [TREImport parseActivity:track withDate:date];
            [xmlData appendString:[TREExportTCX serializeActivity:activity]];
             
            [xmlData appendString:@"\t</Activities>\n"];
            [xmlData appendString:@"</TrainingCenterDatabase>\n"];
            
            int retry = 0; 
            NSString *filename;
            NSString *counter = @"";
            NSFileManager *fileManager = [[NSFileManager alloc] init];
            do { 
                filename = [NSString stringWithFormat:@"%@/%@%@.tcx", [_outFileName stringValue], dateForFilename, counter];
                counter = [NSString stringWithFormat:@"-%d", ++retry];
            } while (retry < MAX_RETRYS && [fileManager fileExistsAtPath:filename]);
            
            if (retry >= MAX_RETRYS && [fileManager fileExistsAtPath:filename]) {
                [self alert:[NSString stringWithFormat:@"Could not write file '%@' after %d retrys. Please move old files out of the way!", filename, retry]];
                break;
            }
            
            NSError *eWriteXml;
            [xmlData writeToFile:filename atomically:true encoding:NSUTF8StringEncoding error:&eWriteXml];
            //NSLog(@"written to file=%@ error=%@", filename, eWriteXml);
            
            [_processBar incrementBy:1];
            [_processBar displayIfNeeded];
            
        }
    } else {
        [self alert:@"Could not read activities from activity store"];
        return;
    }
    
    sqlite3_finalize(statement);
    res = sqlite3_close(database);

    [self finished];
    
}

@end
